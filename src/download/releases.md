# Download Releases

Veloren is a cross-platform game and can run on Windows, Linux and Mac OS.
We provide pre-compiled versions of the game for those platforms on <https://www.veloren.net/welcome>.

Download the version for your operating system and extract it, then check the [Dependencies](./dependencies.md)
